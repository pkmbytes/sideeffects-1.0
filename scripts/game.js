var directionArray = ["Up", "Down", "Right", "Left"];
var colors = ["#EC407A", "#8E24AA", "#C2185B", "#673AB7", "#3F51B5", "#2196F3", "#3D5AFE", "#009688", "#43A047", "#E65100", "#E64A19"];
var timer;
var totalScore = 0;
var sideItems = [];
var timerSeconds = 800;
var elementCreateInterval = 3000;
var gamelevel = 1;
var life = 3;
var initialTopPosition = 60;
var increasingTopPosition = 10;
var audioElement;
var positionArray = [];
var creationTimer;
var enableSound = true;
var enableVibration = true;
var highScore = 0;

function setHighScore() {
    var previousHighScore = localStorage.getItem("sidEffectsHighScore")
    if (previousHighScore) {
        if (previousHighScore < totalScore) {
            localStorage.setItem("sidEffectsHighScore", totalScore);
        }
    } else {
        localStorage.setItem("sidEffectsHighScore", totalScore);
    }
}

function newElementCreateInterval() {
    var rand = Math.round(Math.random() * (elementCreateInterval)) + 500;
    creationTimer = setTimeout(function () {
        createElement(creationTimer);
        increaseCreateElementInterval();
        newElementCreateInterval();
    }, rand);
}

function increaseCreateElementInterval() {
    if (totalScore + 1 % 25 === 0 && elementCreateInterval >= 501) {
        elementCreateInterval -= 200;
        clearInterval(creationTimer);
    }
}

function setRandomDirection() {
    timer = setInterval(function () {
        increasePosition();
    }, timerSeconds);
}

function increasePosition() {
    var directionLableHeight = $(".directionLblCont").height();
    var bodyContainerHeight = $(".bodyContainer").height();
    var bodyContainerTop = $(".bodyContainer").offset().top;
    var footerContainerHeight = $(".footerContainer.row").height();
    var isLimitReached = false;
    if ($(".directionLblCont").length) {
        if ($(".directionLblCont").last().offset().top >= bodyContainerHeight + bodyContainerTop - directionLableHeight - footerContainerHeight) {
            isLimitReached = true;
        }
    }
    if (isLimitReached) {
        endGame();
    } else {
        $(".directionLblCont").each(function (index) {
            var currentTopPosition = parseInt($(this).css("top")) + 10 + "px";
            $(this).css("top", currentTopPosition);
        });
    }
}

function clearTimer(timerAttr) {
    if (timerAttr) {
        clearTimeout(creationTimer);
    } else {
        clearInterval(timer);
        clearTimeout(creationTimer);
    }

}

function registerEvents() {
    $(".right, .left, .up, .down").off("click").on("click", function () {
        calculatePoints(this);
    });
    $(".pause").off("click").on("click", function () {
        pause();
    });
    $("#continueBtn").off("click").on("click", function () {
        $("#continueBtn").hide();
        $("#game-overlay").hide();
        setRandomDirection();
        newElementCreateInterval();
    });

    $("#setMusic").off("click").on("click", function () {
        if ($(this).hasClass("volume-on")) {
            $(this).removeClass("volume-on").addClass("volume-off")
            enableSound = false;
        } else {
            $(this).removeClass("volume-off").addClass("volume-on")
            enableSound = true;
        }
    });

    $("#play").off(
        "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend").on(
        "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend",
        function () {
            $(this)
                .removeClass("bounce animated");
        }
    );
    $(".orgName").off(
        "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend").on(
        "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend",
        function () {
            $(this).removeClass("jello animated");
            clearInterval(window.setLogoAnimation);
            $(".ceyoneLogoContainer").hide();
        }
    );
    $("#levelWrapper").off(
        "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend").on(
        "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend",
        function () {
            $(this)
                .removeClass("slideInUp animated").hide();
        }
    );
}

function endGame() {
    clearTimer();
    setHighScore();
    $("#HighScore").html(localStorage.getItem("sidEffectsHighScore"));
    $("#score").html($(".scoreCard").html());
    $("#game-overlay").show();
    $("#gameOverWrapper").show();
}

function calculatePoints(element) {
    $(".directionBtn").addClass("disabled");
    var btnValue = $(element).attr("value");
    if (sideItems.indexOf(btnValue) != -1) {
        playSound()
        $(".scoreCard").html(++totalScore);
        levelUp(totalScore);
        var sideItemIndex = sideItems.indexOf(btnValue);
        sideItems.splice(sideItemIndex, 1);
        $($(".directionLblCont[data-direction=" + btnValue + "]").last()).remove();
        if (sideItems.length == 0)
            createElement();
        if (totalScore % 25 === 0 && timerSeconds >= 201) {
            increaseTimer();
        }
    }
}

function levelUp(totalScore) {
    if (totalScore % 25 == 0) {
        gamelevel = parseInt(totalScore / 25) + 1;
        $(".level").html(gamelevel);
        $("#levelWrapper").show();
        $("#levelWrapper").addClass("slideInUp animated");
    }
}

function createElement() {
    var direction = directionArray[Math.floor(Math.random() * directionArray.length)];
    var newElement = $('<div class="directionLblCont" data-direction=' + direction + '><div class="directionIcon arrow ' + direction + ' directionBtn" value=' + direction + '></div><span class="directionLable">' + direction + '</span></div>');
    var eleClasses = ["directionIcon", "directionLable"];
    var randomClass = eleClasses[Math.floor(Math.random() * 2)];
    newElement.find("." + randomClass).hide();
    var curColor = colors[Math.floor(Math.random() * colors.length)];
    $(newElement).css("background-color", curColor);
    $(newElement).hide();
    $('.bodyContainer').prepend(newElement);
    positionElement(newElement);
    sideItems.push(direction);
}

function positionElement(ele) {
    $(ele).css("left", positionArray[Math.floor(Math.random() * positionArray.length)] + 10 + "px").show();
}

function createAudioElement() {
    audioElement = document.createElement('audio');
    audioElement.setAttribute('src', "sounds/dissolve.mp3");
}

function playSound() {
    if (enableSound) {
        audioElement.currentTime = 0;
        audioElement.play();
    }
}

function increaseTimer() {
    timerSeconds -= 200;
    clearTimer();
    setRandomDirection();
    newElementCreateInterval();
}

function reStart() {
    totalScore = 0;
    sideItems = [];
    timerSeconds = 800;
    gamelevel = 1;
    life = 3;
    initialTopPosition = 60;
    increasingTopPosition = 10;
    elementCreateInterval = 3000;
    audioElement;
    positionArray = [];
    creationTimer;
    $(".directionLblCont").remove();
    $(".level").html("1");
    $(".scoreCard").html("0")
    $("#game-overlay").show();
    $("#myModal").show();
    $("#gameOverWrapper").hide();
    window.getStart = setInterval(countDown, 1000);
}

function home() {
    location.reload();
}

function pause() {
    clearTimer();
    $("#game-overlay").show();
    $("#playWrapper").addClass("pause").show();
    $("#homebtn").show();
}